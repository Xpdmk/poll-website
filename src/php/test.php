<?php $reply1 = array(
    'id' => 11,
    'replyOption' => "Salatut elämät",
    'voteCount' => 4,
    'voteDates' => array("2015-03-25", "2015-03-25", "2015-03-28", "2015-04-01"));

$reply2 = array(
    'id' => 12,
    'replyOption' => "Kauniit ja rohkeat",
    'voteCount' => 2,
    'voteDates' => array("2015-03-28", "2015-04-01"));

$reply3 = array(
    'id' => 13,
    'replyOption' => "Huomenta Suomi",
    'voteCount' => 5,
    'voteDates' => array("2015-03-25", "2015-03-26", "2015-03-26", "2015-03-30", "2015-04-02"));    

$data = array(
    'id' => 4,
    'date' => "2015-03-25",
    'question' => 'What is your favourite tv show?',
    'replyOptions' => array($reply1, $reply2, $reply3)
    );

   $json = json_encode($data);

echo $json;

// if ($_GET['category'] == "popular") {
// }

?>