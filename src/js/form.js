


// onclick="document.getElementById('new-poll-dialog').style.display = 'block'" 

(function(){

$('#new-poll-dialog').on('show.bs.modal', function () {
   
})    


    // Adds a new text field for a new answer option to the form
    
    document.getElementById('addOptionInput').addEventListener('click', addInputField);
    
    function addInputField(){
        
        var n = document.querySelectorAll('[placeholder="Type option here"]').length +1;
        
        if (n >= 3) {
            var btns = document.querySelectorAll('.removeThisOption.disabled');
            for(var i = 0; i<btns.length; i++)
            {
             btns[i].addEventListener('click', removeThisOption);
             btns[i].setAttribute('class', 'btn btn-default removeThisOption'); 
            }
        }
        
        if (n >= 6) {
            console.log(this);
            this.setAttribute('class', 'btn btn-success disabled');
        }
        
        var formGroup = document.createElement('div');
        formGroup.setAttribute('class', 'form-group');
        
        var label = document.createElement('label');
        label.setAttribute('for', 'options['+n+']');
        label.textContent = "Option " + n;

        var inputGroup = document.createElement('div');        
        inputGroup.setAttribute('class', 'input-group');

        var input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('class', 'form-control');
        input.setAttribute('name', 'options['+n+']');
        input.setAttribute('placeholder', 'Type option here');
        
        var inputGroupBtn = document.createElement('div');
        inputGroupBtn.setAttribute('class', 'input-group-btn');
        
        var removeBtn = document.createElement('button');
        removeBtn.setAttribute('type', 'button');
        removeBtn.setAttribute('class', 'btn btn-default removeThisOption'); //TODO Nappi toimimaan
        removeBtn.addEventListener('click', removeThisOption);
        
        var glyphicon = document.createElement('i');
        glyphicon.setAttribute('class', 'glyphicon glyphicon-remove');


        removeBtn.appendChild(glyphicon);
        inputGroupBtn.appendChild(removeBtn);

        inputGroup.appendChild(input);
        inputGroup.appendChild(inputGroupBtn);
        
        formGroup.appendChild(label);
        formGroup.appendChild(inputGroup);

        // this = <a><span></span></a>
        
        this.parentNode.insertBefore(formGroup, this);
        
        formGroup.querySelector('input').focus();

    }//end eventlistener for adding textfield 
        
    

    function removeThisOption(event) {
        
        var thisBtn = event.currentTarget;
        var thisOption = event.currentTarget.parentElement.parentElement.parentElement;

        thisBtn.removeEventListener('click', removeThisOption);
        thisOption.parentElement.removeChild(thisOption);
        
        renumberInputs();
    }
    
    function renumberInputs(){
        var optInputs = document.querySelectorAll('[placeholder="Type option here"]');
        var optRmvBtnShouldBeActive = (optInputs.length > 2) ? true : false;

        var addBtn = document.getElementById('addOptionInput');
        addBtn.setAttribute('class', 'btn btn-success');
        addBtn.addEventListener('click', addInputField);

        for (var i = 0; i < optInputs.length; i++ ) 
        {
            var n = i+1;    
            var thisInput = optInputs[i];
            thisInput.setAttribute('name', 'options['+n+']');
            
            var thisFormGroup = thisInput.parentElement.parentElement;
            var thisLabel = thisFormGroup.querySelector('label');
            thisLabel.setAttribute('for', 'options['+n+']');
            thisLabel.textContent = "Option " + n;
            
            var thisRmvBtn = thisFormGroup.getElementsByClassName('removeThisOption')[0];
            if(!optRmvBtnShouldBeActive) {
                
                thisRmvBtn.setAttribute('class', 'btn btn-default removeThisOption disabled');
                thisRmvBtn.removeEventListener('click', removeThisOption);

            } else {
                thisRmvBtn.setAttribute('class', 'btn btn-default removeThisOption');
                thisRmvBtn.addEventListener('click', removeThisOption);
            }

        }
        

        
    }
    
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (this.value.length >= 1)
            {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value);
                } else {
                    o[this.name] = this.value;
                }
            }
        });
        return o;
    };
    
   
        $('#new-poll-dialog form').submit(function(evt) {
//            $('#result').text(JSON.stringify($('form').serializeObject()));
            evt.preventDefault();
            
            var formData = $('#new-poll-dialog form').serializeObject();
            
            if (Object.keys(formData).lastIndexOf('question') == -1 || Object.keys(formData).length < 3)  
            {
                //flash red bg-color on instruction, if formData doesnt contain a question, or if there are not enough replyOptions set
                
                $('#new-poll-dialog .modal-header p').addClass("warning-bg");
                setTimeout(function(){$('p.warning-bg').removeClass("warning-bg");}, 500);
            } else 
            {
                $.ajax({
                    url: 'php/pdo.php',
            		dataType: "json",
                    data: $('#new-poll-dialog form').serializeObject(),
                    type: "POST",
                    success: function(data){
         
                        //close modal
                        $('#new-poll-dialog').modal('hide');
                        
                        //view poll just created   
                        changeCategory("newest");
                        console.log(data);
                        }
                    });
            }
            return false;
        });
    
    
})();