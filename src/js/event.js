/*Not in use. 
Found better implementation of switching tab content*/
'use strict';

function tabclick(element) {
	var tabs = element.parentElement.parentElement.parentElement;
	var contentDivList = tabs.querySelector("#tab-content").children;
	for (var i = 0; i < contentDivList.length; i++) {
		if (element.innerHTML == contentDivList[i].id) {
			contentDivList[i].style.display = "block";
		} else {
			contentDivList[i].style.display = "none";
		}
	}
	
	var ulElement = element.parentElement.parentElement;
	var liElements = ulElement.children;
	for (var i = 0; i < liElements.length; i++) {
		var li = liElements[i];
		li.setAttribute("class", "");
		var a = li.childNodes[1];
		a.setAttribute("aria-expanded", "false");
	}
	var parent = element.parentElement;
	parent.setAttribute("class", "active");
	element.setAttribute("aria-expanded", true);
	document.querySelector("#tabs ul li a[aria-expanded=false]").onclick = function()  {tabclick(this)};
}


document.querySelector("#tabs ul li a[aria-expanded=false]").onclick = function()  {tabclick(this)};