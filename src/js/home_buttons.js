'use strict';

$(window).on("load", function(){

	var $body = $("body");
	//console.log($body);	
	//event listeners for ajax events: display modal with spinner-icon (via css) during ajax loading 
	$(document).on({

	    ajaxStart: function() { $body.addClass("loading"); }, 
	    ajaxStop: function() { $body.removeClass("loading"); }    
	});


	// SET EVENT HANDLERS FOR CATEGORY BUTTONS 
    var categoryButtons = document.querySelectorAll(".category");

    for (var i = 0; i < categoryButtons.length; i++) 
    {
    	(function(button){
	    	var element = button;
    		var category = element.textContent.toLowerCase();
        	element.onclick = function() {
        		changeCategory(category);
        	};
    		
    	})(categoryButtons[i]);
    	
    
    }
});

function changeCategory(category) {

	$("#category-buttons").children().removeClass('active');
	$(".category-"+category).addClass('active');

	$("#polls").children().hide();

	$("#"+category).empty();
	$("#"+category).show();
	
	insertPoll({category: category, howMany: 3, currPage: 1});
	
	//check which answering mode we are in


}






// Function for answering mode switch at the navigation bar
function toggleSwitch (element) {
	var sw = element;
//	console.log(element);
	var pollContainers = document.getElementsByClassName("poll-container");
	var displayStyleResults;
	var displayStyleAnswer;
	
	// Switch ball is on the left, show answer forms
	if (sw.className == "switch-active slider round") {
		
		sw.setAttribute("class", "slider round");

		$('.nav-results').removeClass('active');
		$('.nav-answer').addClass('active');
		$('.nav-tabs li.answer').children('a').tab('show');

		// displayStyleAnswer = "block";
		// displayStyleResults = "none";
	// Switch ball is on the right, show result charts
	} else {
		sw.setAttribute("class", "switch-active slider round"); //pallo oikealla
	
		$('.nav-answer').removeClass('active');
		$('.nav-results').addClass('active');
		$('.nav-tabs li.results').children('a').tab('show');
		// displayStyleResults = "block"; 
		// displayStyleAnswer = "none";
	}
	for (var i = 0; i < pollContainers.length; i++) {
		var pollContainer = pollContainers[i];
		var pollContainerChildren = pollContainer.children;
		for (var k = 0; k < pollContainerChildren.length; k++) {
			// Doesn't hide the poll question
			if (pollContainerChildren[k].tagName == "H3") {
					continue;
			} else if (pollContainerChildren[k].className == "pollAnswerForm") {
//				pollContainerChildren[k].style.display = displayStyleAnswer;
			} else {
//				pollContainerChildren[k].style.display = displayStyleResults;
			}
		}
	}
	
//	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
}