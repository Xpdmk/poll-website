SET FOREIGN_KEY_CHECKS=0; 

DROP TABLE IF EXISTS ReplyOptions;
DROP TABLE IF EXISTS Votes;
DROP TABLE IF EXISTS Polls;

FLUSH TABLES;
SET FOREIGN_KEY_CHECKS=1; 


CREATE TABLE Polls(
	p_id INT NOT NULL AUTO_INCREMENT,
	question VARCHAR(255),
	p_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (p_id)
);


CREATE TABLE ReplyOptions(
	o_id INT NOT NULL AUTO_INCREMENT,
	poll_id INT,
	replyOption VARCHAR(155),

	PRIMARY KEY (o_id),
	FOREIGN KEY (poll_id) REFERENCES Polls(p_id)
);

CREATE TABLE Votes(
	v_id INT NOT NULL AUTO_INCREMENT,
	v_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	option_id INT,

	PRIMARY KEY (v_id),
	FOREIGN KEY (option_id) REFERENCES ReplyOptions(o_id)
); 

-- TIMESTAMP - format: YYYY-MM-DD HH:MI:SS




-- SAMPLE POLL::


INSERT INTO Polls (question) VALUES ("What is your favourite animal?"); 

# ADDING REPLY OPTIONS FOR POLL:

INSERT INTO ReplyOptions (replyOption, poll_id) 
SELECT "Dog", Polls.p_id 
FROM Polls 
WHERE (Polls.question="What is your favourite animal?");

INSERT INTO ReplyOptions (replyOption, poll_id) 
SELECT "Cat", Polls.p_id 
FROM Polls 
WHERE (Polls.question="What is your favourite animal?");

INSERT INTO ReplyOptions (replyOption, poll_id) 
SELECT "Bird", Polls.p_id 
FROM Polls 
WHERE (Polls.question="What is your favourite animal?");

INSERT INTO ReplyOptions (replyOption, poll_id) 
SELECT "Horse", Polls.p_id 
FROM Polls 
WHERE (Polls.question="What is your favourite animal?");

-- End option adding


-- ADD A FEW Votes


INSERT INTO Votes (option_id) VALUES (2);
INSERT INTO Votes (option_id) VALUES (2);
INSERT INTO Votes (option_id) VALUES (2);
INSERT INTO Votes (option_id) VALUES (3);
INSERT INTO Votes (option_id) VALUES (1);
INSERT INTO Votes (option_id) VALUES (1);
INSERT INTO Votes (option_id) VALUES (3);
INSERT INTO Votes (option_id) VALUES (3);
INSERT INTO Votes (option_id) VALUES (1);
INSERT INTO Votes (option_id) VALUES (4);
INSERT INTO Votes (option_id) VALUES (3);
INSERT INTO Votes (option_id) VALUES (4);
INSERT INTO Votes (option_id) VALUES (2);

-- END replies